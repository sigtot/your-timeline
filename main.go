package main

import (
	"bufio"
	"os"
	"strings"
	"regexp"
	"strconv"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"log"
	"github.com/nu7hatch/gouuid"
	"errors"
	//"github.com/teris-io/shortid"
	"mime/multipart"
	"io/ioutil"
)

const webRoot = "./www"
const apiPath = "/api"
const uploadPath = "/upload"
const fileDir = "./files/"

const batchSize = 50000

type User struct {
	f 		*os.File // Pointer to their file to keep track of cursor position
	files	[]*os.File
	id		string
}

type Session struct {
	users 	[]User
	id 		string
}

type BatchData struct {
	Batch 	[][3]int 	`json:"batch"`
	Last 	bool 		`json:"last"`
}

var users = make(map[string] User)
var sessions = make(map[string] Session)

func main() {
	startServer()
}
func staticHandler(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Path
	if r.Method == "GET" && path != apiPath {
		if path := r.URL.Path; path == "/" {
			http.ServeFile(w, r, webRoot+"/index.html")
		} else {
			if r.Method == "GET" {
				http.ServeFile(w, r, webRoot+path)
			}
			return
		}
	} else {
		http.Error(w, "405 method not allowed", http.StatusMethodNotAllowed)
	}
}

func uploadHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, "405 method not allowed", http.StatusMethodNotAllowed)
	}

	file, handle, err := r.FormFile("file")
	if err != nil {
		fmt.Fprintf(w, "Could not read file from form: %v\n", err)
		return
	}
	defer file.Close()

	mimeType := handle.Header.Get("Content-Type")
	if mimeType == "application/json" || mimeType == "text/plain" {
		err = saveFile(w, file, handle)
		if err == nil {
			output, err := json.Marshal("Successful upload")
			if err == nil {
				w.Write(output)
			} else {
				fmt.Printf("Could not marshal json: %v\n", err)
				http.Error(w, "500 internal server error", http.StatusInternalServerError)
			}
		} else {
			fmt.Printf("Could not save file: %v\n", err)
			http.Error(w, "500 internal server error", http.StatusInternalServerError)
		}
	} else {
		fmt.Printf("User did not upload a plaintext or json file: Mimetype %s\n", mimeType)
		http.Error(w, "400 bad request", http.StatusBadRequest)
	}

}

func saveFile(w http.ResponseWriter, file multipart.File, handle *multipart.FileHeader) error{
	data, err := ioutil.ReadAll(file)
	if err == nil {
		err = ioutil.WriteFile(fileDir + "user-id" + ".json", data, 0666)
	}
	return err
}

func apiHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, "405 method not allowed", http.StatusMethodNotAllowed)
	}
	msg := r.FormValue("msg")

	w.Header().Set("content-type", "application/json")

	err := error(nil)
	output := []byte(nil)

	switch msg {
	case "begin":
		userId, err := uuid.NewV4()
		if err != nil {
			fmt.Printf("Could not generate uuid: %s", err)
			break
		}

		f, err := os.Open("Location History.json")
		if err != nil {
			f.Close()
			fmt.Printf("Could not open location history file: %s", err)
			break
		}

		users[userId.String()] = User{f, []*os.File{f}, userId.String()}

		output, err = json.Marshal(userId.String())
	case "batch":
		id := r.FormValue("uuid")
		if user, ok := users[id]; ok {
			batchData := BatchData{}
			batchData.Batch, batchData.Last = parseBatch(user)
			output, err = json.Marshal(batchData)
		} else {
			http.Error(w, "400 bad request", http.StatusBadRequest)
		}
	default:
		http.Error(w, "400 bad request", http.StatusBadRequest)
	}

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(output)
}

func getStringSubMatches(s string, pattern *regexp.Regexp) ([]string, error) {
	matches := pattern.FindStringSubmatch(s)
	err := error(nil)
	if len(matches) == 1 {
		// No match (only empty string)
		err = errors.New("No regexp match found except empty string")
	}

	if len(matches) < 1 {
		err = errors.New("No regexp match found")
	}
	return matches, err
}

func parseBatch(user User) ([][3]int, bool) {
	reader := bufio.NewReader(user.f)
	timeRe := regexp.MustCompile(`"timestampMs" : "(.*?)",`)
	locRe := regexp.MustCompile(`itudeE7" : (.*?),`)

	var batch [batchSize] [3]int
	n := 0
	for n < batchSize {
		buf, _, err := reader.ReadLine()
		if err != io.EOF {
			if err != nil {
				fmt.Printf("Failed to read line: %s\n", err)
			}

			if strings.Compare(string(buf), "  }, {") == 0 {
				// Finding "  }, {" means the following three lines will be time, latitude and longitude
				var a [3] int

				// Extract time
				buf, _, err = reader.ReadLine()
				if err != nil {
					fmt.Printf("Failed extract time: %s\n", err)
				}
				matches, err := getStringSubMatches(string(buf), timeRe)
				if err != nil {
					continue
				}

				timeS := matches[1]
				timeS = timeS[:len(timeS)-3] // Convert ms to s
				a[0], err = strconv.Atoi(timeS)
				if err != nil {
					fmt.Printf("Couldn't convert string to int: %s\n", err)
				}

				// Extract latitude and longitude
				for j := 1; j < 3; j++ {
					buf, _, err = reader.ReadLine()
					if err != nil {
						fmt.Printf("Failed to extract latlon: %s\n", err)
					}
					matches, err := getStringSubMatches(string(buf), locRe)
					if err != nil {
						continue
					}
					a[j], err = strconv.Atoi(matches[1])
					if err != nil {
						fmt.Printf("Couldn't convert string to int: %s\n", err)
					}
				}

				if a != [3]int{0, 0, 0} {
					batch[n] = a
					n++
				}
			}
		} else {
			j, err := json.Marshal(batch)
			if err != nil {
				fmt.Printf("Couldn't encode json: %s\n", err)
			}
			fmt.Printf("Last batch done with length %d\n", len(string(j)))
			user.f.Close()
			return batch[:n], true
		}
	}

	j, err := json.Marshal(batch)
	if err != nil {
		fmt.Printf("Couldn't encode json: %s\n", err)
	}
	fmt.Printf("Batch done with length %d\n", len(string(j)))
	return batch[:n], false
}

func startServer() {
	mux := http.NewServeMux()

	mux.HandleFunc("/", staticHandler)
	mux.HandleFunc(apiPath, apiHandler)
	mux.HandleFunc(uploadPath, uploadHandler)

	err := http.ListenAndServe(":9090", mux)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
