$("#fileForm").submit(function(){
    var formData = new FormData();
    formData.append("file", $("#file")[0].files[0]);

    $.ajax({
        url : "/upload",
        type : "POST",
        data : formData,
        processData: false,
        contentType: false,
        success : function(msg) {
            console.log(msg);
        }
    });
});
