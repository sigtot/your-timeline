$("document").ready(function(){
    getData();
});

var locationData = [];

function getData() {
    $.ajax({
        url: "/api",
        method: "POST",
        dataType: "json",
        data: {msg:"begin"}
    }).done(function(res){getBatches(res)});
}

function getBatches(uuid) {
    var data = {
        msg: "batch",
        uuid: uuid
    };

    $.ajax({
        url: "/api",
        method: "POST",
        dataType: "json",
        data: data
    }).done(function(res){getBatch(res, uuid)});
}

function getBatch(res, uuid) {
    locationData = locationData.concat(res["batch"]);

    var data = {
        msg: "batch",
        uuid: uuid
    };

    $.ajax({
        url: "/api",
        method: "POST",
        dataType: "json",
        data: data
    }).done(function(res){
        if(!res["last"]) getBatch(res, uuid);
        else locationData = locationData.concat(res["batch"]);
    });
}
